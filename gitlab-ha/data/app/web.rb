roles ['application_role']
nginx['enable'] = true
postgresql['enable'] = false
gitlab_rails['db_host'] = 'pgbouncer'
gitlab_rails['db_port'] = 6432
gitlab_rails['db_password'] = 'thisisabadpassword'
gitlab_rails['auto_migrate'] = false
redis['master_name'] = 'gitlab-redis'
redis['master_password'] = 'password'
gitlab_rails['redis_sentinels'] = [
  {'host' => 'redis1', 'port' => 26379},
  {'host' => 'redis2', 'port' => 26379},
  {'host' => 'redis3', 'port' => 26379}
]
user['uid'] = 9000
user['gid'] = 9000
web_server['uid'] = 9001
web_server['gid'] = 9001
registry['uid'] = 9002
registry['gid'] = 9002
