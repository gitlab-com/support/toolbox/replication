FROM ubuntu:latest
RUN apt-get update
RUN apt-get install -y curl openssh-server ca-certificates tzdata
RUN curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | bash
RUN apt-get install -y gitlab-ee
COPY scripts/app/web2/ /assets/
COPY data/app/web.rb /etc/gitlab/gitlab.rb
COPY data/gitlab-secrets.json /etc/gitlab/gitlab-secrets.json
COPY data/seed.rb /tmp/seed.rb
RUN /assets/setup
ENTRYPOINT /assets/wrapper
