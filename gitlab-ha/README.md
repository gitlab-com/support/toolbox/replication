# gitlab-ha

This deploys a pre-seeded GitLab Horizontally scaled environment, which consists
of 13 containers:

* 2 app nodes
* 3 db nodes
* 3 consul nodes
* 3 redis/sentinel nodes
* 1 pgbouncer node
* 1 load balancer node (using haproxy)

Because of the number of containers, I do not recommend running this on your
local computer. It is better to spin up a [32gb Digital Ocean droplet](https://gitlab.com/gitlab-com/dev-resources/-/blob/master/dev-resources/README.md#creating-a-droplet-with-digital-ocean)
or similar VM/server, install Docker on it, and run it from there.

## Using this

* To start it up, you would use the command: `docker-compose up`
* To stop it, you would use the command: `docker-compose down -v`

## NOTE

You are likely to see errors in the compose output. These can largely be
ignored. If when using the GitLab UI you see a 500 error, it is likely due to
the flakey nature of docker network. Refresh a few times and it should go away.
Once it is fully seeded and such, it will perform much better.

## Accessing the GitLab instance

You can access the GitLab instance via http://localhost

If you are running this on a VM/server, you would instead want to use
http://<server_ip>, replacing `<server_ip>` with the server's public IP
address.

The root user credentials are:

| Username | Password   |
|----------|------------|
| root     | `password` |

You might see 502 errors at first. If so, give it a bit to run the initial crons and such and then refresh the page.

## Seeds

All GitLab images/builds are pre-seeded to help with replication. In them you should see:

* 27 users
* 3 groups (with 3 subgroups per group)
  * 10 labels per group/subgroup
* 26 keys
* 39 projects
  * 5 milestones per project
  * 5 labels per project
  * 5 issues per
    * 1 comment per issue
* MRs
  * 1 comment per MR
* 50 personal snippets
* 10 or so forks

If some are missing, it is not a big deal. A lot of random allocation is in place (permissions,
visibilities, etc.), so some seeds might have failed. Still should present a decent data set
to work with.

## LDAP/SAML users

| Username | GitLab Password | LDAP/SMAL Password | Email |
|----------|-----------------|--------------------|-------|
| alice | password | password | alice@example.com |
| bob | password | password | bob@example.com |
| charles | password | password | charles@example.com |
| dylan | password | password | dylan@example.com |
| ethan | password | password | ethan@example.com |
| fred | password | password | fred@example.com |
| george | password | password | george@example.com |
| heather | password | password | heather@example.com |
| inga | password | password | inga@example.com |
| jacob | password | password | jacob@example.com |
| kyle | password | password | kyle@example.com |
| lisa | password | password | lisa@example.com |
| mike | password | password | mike@example.com |
| nate | password | password | nate@example.com |
| olivia | password | password | olivia@example.com |
| penelope | password | password | penelope@example.com |
| quincy | password | password | quincy@example.com |
| rachel | password | password | rachel@example.com |
| steve | password | password | steve@example.com |
| todd | password | password | todd@example.com |
| ulysses | password | password | ulysses@example.com |
| vivian | password | password | vivian@example.com |
| wayne | password | password | wayne@example.com |
| xavier | password | password | xavier@example.com |
| yvonne | password | password | yvonne@example.com |
| zach | password | password | zach@example.com |
