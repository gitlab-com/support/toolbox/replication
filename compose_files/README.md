# Compose Files

| Compose File    | Command to run                         | What it does                                |
|-----------------|----------------------------------------|---------------------------------------------|
| gitlab-ldap.yml | `IMAGE_TAG=latest docker compose -f gitlab_ldap.yml up` | Deploys GitLab and LDAP, integrates the two |
| gitlab-saml.yml | `IMAGE_TAG=latest docker compose -f gitlab_saml.yml up` | Deploys GitLab and SAML, integrates the two |

Replace `latest` with a [GitLab tag](https://gitlab.com/gitlab-org/gitlab/-/tags) (like `v15.4.0-ee`) to start a corresponding GitLab version. There is a number of pre-built containers, check container registry projects:

* [jcolyer/gitlab-ldap](https://gitlab.com/gitlab-com/support/toolbox/replication/container_registry/667841)
* [jcolyer/gitlab-saml](https://gitlab.com/gitlab-com/support/toolbox/replication/container_registry/667855)

## Building container with required GitLab version

If the version you need does not exits yet, manually start a pipeline from **Pipelines** -> **Run pipeline** on a master branch specifying a GitLab version as `INPUT_GL_TAG` variable.

## Accessing the GitLab instance

You can access the GitLab instance via http://localhost

The root user credentials are:

| Username | Password   |
|----------|------------|
| root     | `5iveL!fe` | 

You might see 502 errors at first. If so, give it a bit to run the initial crons and such and then refresh the page.

## Seeds

All GitLab images/builds are pre-seeded to help with replication. In them you should see:

* 27 users
* 3 groups (with 3 subgroups per group)
  * 10 labels per group/subgroup
* 26 keys
* 39 projects
  * 5 milestones per project
  * 5 labels per project
  * 5 issues per 
    * 1 comment per issue
* MRs
  * 1 comment per MR
* 50 personal snippets  
* 10 or so forks

If some are missing, it is not a big deal. A lot of random allocation is in place (permissions,
visibilities, etc.), so some seeds might have failed. Still should present a decent data set
to work with.

## LDAP/SAML users

| Username | GitLab Password | LDAP/SMAL Password | Email |
|----------|-----------------|--------------------|-------|
| alice | 5iveL!fe | password | alice@example.com |
| bob | 5iveL!fe | password | bob@example.com |
| charles | 5iveL!fe | password | charles@example.com |
| dylan | 5iveL!fe | password | dylan@example.com |
| ethan | 5iveL!fe | password | ethan@example.com |
| fred | 5iveL!fe | password | fred@example.com |
| george | 5iveL!fe | password | george@example.com |
| heather | 5iveL!fe | password | heather@example.com |
| inga | 5iveL!fe | password | inga@example.com |
| jacob | 5iveL!fe | password | jacob@example.com |
| kyle | 5iveL!fe | password | kyle@example.com |
| lisa | 5iveL!fe | password | lisa@example.com |
| mike | 5iveL!fe | password | mike@example.com |
| nate | 5iveL!fe | password | nate@example.com |
| olivia | 5iveL!fe | password | olivia@example.com |
| penelope | 5iveL!fe | password | penelope@example.com |
| quincy | 5iveL!fe | password | quincy@example.com |
| rachel | 5iveL!fe | password | rachel@example.com |
| steve | 5iveL!fe | password | steve@example.com |
| todd | 5iveL!fe | password | todd@example.com |
| ulysses | 5iveL!fe | password | ulysses@example.com |
| vivian | 5iveL!fe | password | vivian@example.com |
| wayne | 5iveL!fe | password | wayne@example.com |
| xavier | 5iveL!fe | password | xavier@example.com |
| yvonne | 5iveL!fe | password | yvonne@example.com |
| zach | 5iveL!fe | password | zach@example.com |


## Querying/modifying LDAP db on a running container

To change LDAP db before starting the container, use `data/bootstrap.ldif` file. However, if you need to modify the db on the fly when the container is already runing, use `ldapdelete`, `ldapmodify` commands executed from within the `ldap` container:

```bash
docker exec -it  compose_files_ldap_1 bash
```

LDAP DN `cn=admin,dc=example,dc=com` has password set to `admin`. Use it as password when prompted for one. 

### To remove a LDAP group member

1. Save thie .ldif file, replacing **(1)** group's `dn` and **(2)** `member:` value with member's `dn`:

  ```
  # cat > /tmp/groupmod.ldif <<EOF
  dn: cn=admins,ou=groups,dc=example,dc=com
  changetype: modify
  delete: member
  member: uid=ethan,ou=admin,dc=example,dc=com
  EOF
  ```

1. Run ldapmodify command:

  ```
  ldapmodify -H ldap://localhost:389 -D "cn=admin,dc=example,dc=com" -W -f /tmp/groupmod.ldif
  ```

### Deleting a single user:

  ```
  ldapdelete -H ldap://localhost:389 -D "cn=admin,dc=example,dc=com" -W  "uid=ethan,ou=admin,dc=example,dc=com"
  ```

### Querying a single user:

  ```
  ldapsearch -x -b "dc=example,dc=com" -H ldap://localhost:389 -D "cn=admin,dc=example,dc=com" -W "uid=alice"
  ```
